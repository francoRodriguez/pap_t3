<%@page contentType="text/html;charset=UTF-8"%><HTML>
<HEAD>
<TITLE>Methods</TITLE>
</HEAD>
<BODY>
<H1>Methods</H1>
<UL>
<LI><A HREF="Input.jsp?method=2" TARGET="inputs"> getEndpoint()</A></LI>
<LI><A HREF="Input.jsp?method=5" TARGET="inputs"> setEndpoint(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=10" TARGET="inputs"> getControladorPublish()</A></LI>
<LI><A HREF="Input.jsp?method=15" TARGET="inputs"> altaUsuarioDocente(java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.util.Calendar,boolean,java.lang.String,java.lang.String,java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=36" TARGET="inputs"> altaUsuarioEstudiante(java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.util.Calendar,boolean,java.lang.String,java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=55" TARGET="inputs"> rutaAnombreDeImagen(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=60" TARGET="inputs"> listarInstitutosArray()</A></LI>
<LI><A HREF="Input.jsp?method=63" TARGET="inputs"> nuevoCurso(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=68" TARGET="inputs"> setearCurso(java.lang.String,java.lang.String,java.lang.String,int,int,java.lang.String,java.util.Calendar,java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=87" TARGET="inputs"> listarNombreCursos(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=92" TARGET="inputs"> seleccionarDtUsuario(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=111" TARGET="inputs"> listarEdicionesdeCurso(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=116" TARGET="inputs"> seleccionarUsuario(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=139" TARGET="inputs"> seleccionarInscripcionED(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=144" TARGET="inputs"> listarProgramas()</A></LI>
<LI><A HREF="Input.jsp?method=147" TARGET="inputs"> listarUsuarios()</A></LI>
<LI><A HREF="Input.jsp?method=150" TARGET="inputs"> seleccionarCurso(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=171" TARGET="inputs"> modificarDatos(java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.util.Calendar)</A></LI>
<LI><A HREF="Input.jsp?method=184" TARGET="inputs"> cursosDePrograma()</A></LI>
<LI><A HREF="Input.jsp?method=187" TARGET="inputs"> loginEmail(java.lang.String,java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=194" TARGET="inputs"> loginNickname(java.lang.String,java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=201" TARGET="inputs"> inscrpcionesAceptadas(java.lang.String,java.lang.String,java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=210" TARGET="inputs"> traerUltimaEdicion(java.lang.String,java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=229" TARGET="inputs"> obtenerInscripciones(java.lang.String,java.lang.String,java.lang.String,boolean)</A></LI>
<LI><A HREF="Input.jsp?method=240" TARGET="inputs"> confirmarAltaCurso()</A></LI>
<LI><A HREF="Input.jsp?method=243" TARGET="inputs"> mostrarDatosdeCurso(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=264" TARGET="inputs"> mostrarDtEdicionesArray(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=269" TARGET="inputs"> altaInscripcionEdc(java.lang.String,java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=276" TARGET="inputs"> seleccionarCursoInstancia(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=281" TARGET="inputs"> seleccionarInstituto(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=286" TARGET="inputs"> listarCategoriasList()</A></LI>
<LI><A HREF="Input.jsp?method=289" TARGET="inputs"> listarCategoriasPF(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=294" TARGET="inputs"> seleccionarDtDocente(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=301" TARGET="inputs"> cursosdeCategoria(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=306" TARGET="inputs"> dejarSeguirUsuario(java.lang.String,java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=313" TARGET="inputs"> altaPrograma(java.lang.String,java.lang.String,java.util.Calendar,java.util.Calendar,java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=326" TARGET="inputs"> checkSeguidor(java.lang.String,java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=333" TARGET="inputs"> listarCategorias()</A></LI>
<LI><A HREF="Input.jsp?method=336" TARGET="inputs"> usuariosSeguidos(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=341" TARGET="inputs"> verPrograma(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=354" TARGET="inputs"> pruebaDate(java.util.Calendar)</A></LI>
<LI><A HREF="Input.jsp?method=359" TARGET="inputs"> listarDocentes()</A></LI>
<LI><A HREF="Input.jsp?method=362" TARGET="inputs"> seguirUsuario(java.lang.String,java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=369" TARGET="inputs"> institutoDeCurso(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=374" TARGET="inputs"> mostrarEdicion(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=391" TARGET="inputs"> programasdeCurso(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=396" TARGET="inputs"> emailToNick(java.lang.String)</A></LI>
</UL>
</BODY>
</HTML>