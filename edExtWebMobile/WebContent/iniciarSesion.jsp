<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!doctype html>
<html lang="es">
<head>
<%@include file="headerLogin.jsp"%>
<link href="login.css" rel="stylesheet">
</head>
<body class="text-center">
	<form class="form-signin" action="InicioSesion" method="post">
		<img class="mb-4" src="img/icono.png" alt="" width="72" height="72">
		<h1 class="h3 mb-3 font-weight-normal">Iniciar Sesion</h1>
		<label for="inputNick" class="sr-only">Nick Name</label> <input
			type="text" id="inputNick" name="nick" class="form-control"
			placeholder="Nickname" required autofocus> <label
			for="inputPassword" class="sr-only">Password</label> <input
			type="password" id="inputPassword" name="password"
			class="form-control" placeholder="Password" required>
		<button class="btn btn-lg btn-primary btn-block" type="submit">Ingresar</button>
		<%
			//si ls respuesta de mi servlet es diferente a vacio
			if (request.getAttribute("mensajeError") != null) {
				//tendrìa que mandar a imprimir por medio del alert de abajo el mensaje de acceso correcto o incorrecto, pero no me lo muestra
		%>
		<div class="alert alert-danger" role="alert">
			<%=request.getAttribute("mensajeError")%>
		</div>
		<%
			}
		%>
		<p class="mt-5 mb-3 text-muted">&copy; EdExt 2020</p>
	</form>
</body>
</html>