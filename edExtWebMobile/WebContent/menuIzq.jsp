				<ul class="list-group">					
					<li class="list-group-item">
						<h5 class="text-uppercase">Institutos</h5>
						<ul class="list-group list-group-flush">
							<%
							String[] listInstitutos = (String[]) sesion.getAttribute("listInstitutos");
							
							if( listInstitutos != null){
								for (int i=0; i < listInstitutos.length;  i++){
									out.println("<li class='list-group-item'>" + "<a href=\"Institutos?inselecc="+ listInstitutos[i] + "\">" + listInstitutos[i] + "</a>" + "</li>");
								}	
							} else {out.println("<li class='list-group-item'> No hay datos cargados </li>");}
													
							%>
						</ul>
					</li>
					<li class="list-group-item">
						<h5 class="text-uppercase">Categor&iacute;as</h5>
						<ul class="list-group list-group-flush">
													<%		
							ArrayList<String> listCategorias;
							listCategorias = (ArrayList<String>)session.getAttribute("listCategorias");
 
							if( listCategorias != null){
							     for (int counter = 0; counter < listCategorias.size(); counter++) { 		      
							          System.out.println(listCategorias.get(counter));
							          out.println("<li class='list-group-item'>" + "<a href=\"Institutos?catselecc="+ listCategorias.get(counter) + "\">" + listCategorias.get(counter) + "</a>" + "</li>");
							      } 
							} else {out.println("<li class='list-group-item'> No hay datos cargados </li>");}
													
							%>

						</ul>
					</li>		
				</ul>