<%@page import="publicadores.DtCurso"%>
<%@page import="publicadores.DtEdicion"%>
<%@page import="publicadores.DtInscripcionEdc"%>
<%@page import="publicadores.EstadoInscripcionEdc"%>
<%@page import="publicadores.DtUsuario"%>
<%@ page import="java.io.*,java.util.*"%>
<%@ page import="javax.servlet.*,java.text.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>

<%@include file="header.jsp"%>
<meta charset="ISO-8859-1">
<title>Mostrar Cursos</title>
</head>
<body>

	<%
		HttpSession sesion = request.getSession();
		if (sesion.getAttribute("conectado") == null) {
	%><%@include file="navbarSinSession.jsp"%>
	<%
		} else {
	%><%@include file="navbarSession.jsp"%>
	<%
		}
		if (request.getAttribute("inicio") == null && request.getAttribute("inicio") != "vuelve") {
			RequestDispatcher rd;
			request.setAttribute("inicio", "si");
			rd = request.getRequestDispatcher("Cursos");
			rd.forward(request, response);
		}
		DtCurso dtc = (DtCurso) request.getAttribute("dtCurso");
		DtEdicion[] dtediciones = (DtEdicion[]) request.getAttribute("dtediciones");

		String nombreInstituto = (String) request.getAttribute("insSel");
		String nombreCurso = (String) request.getAttribute("curSel");

		boolean docente = false;
		if (sesion.getAttribute("conectado") != null) {
			docente = (boolean) sesion.getAttribute("esDocente");
		} else {
			docente = false;
		}
	%>

	<nav class="navbar navbar-expand-lg navbar-light bg-light d-block d-sm-block d-md-none d-lg-none d-xl-none">
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto ml-auto text-center">
				<li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
					role="button" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false"> Institutos </a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<%
							String[] listInstitutos2 = (String[]) sesion.getAttribute("listInstitutos");

							if (listInstitutos2 != null) {
								for (int i = 0; i < listInstitutos2.length; i++) {
									out.println("<a class='dropdown-item' href='Institutos?inselecc=" + listInstitutos2[i] + "'>"
											+ listInstitutos2[i] + "</a>");
								}
							} else {
								out.println("<a class='dropdown-item'> No hay datos cargados </a>");
							}
						%>



					</div></li>
				<li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
					role="button" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false"> Categor&iacute;as </a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<%
							ArrayList<String> listCategorias2;
							listCategorias2 = (ArrayList<String>) session.getAttribute("listCategorias");

							if (listCategorias2.size() > 0) {
								for (int counter = 0; counter < listCategorias2.size(); counter++) {
									out.println("<a class='dropdown-item' href='Institutos?catselecc=" + listCategorias2.get(counter)
											+ "'>" + listCategorias2.get(counter) + "</a>");
								}
							} else {
								out.println("<a class='dropdown-item'> No hay datos cargados </a>");
							}
						%>
					</div>
				</li>
				<li>
					<div class="d-xl-none d-lg-none d-md-block">
			    		<a class="nav-link d-inline black" href="consultaUsuario.jsp"><%=sesion.getAttribute("nombre")%> <%=sesion.getAttribute("apellido")%>
			    			<img src="imagenes/<%=sesion.getAttribute("nombreImagen")%>" class='img-usuario rounded-circle' alt='Imagen Perfil'> 															
			    		</a>
		    		</div>
				</li>				
				<li>
					<div class="d-xl-none d-lg-none d-md-block">		    		
						<a href='LogOut' class=" nav-link d-inline blue">
				    		Salir
			    		</a>
		    		</div>
				</li>
			</ul>
		</div>
	</nav>
	<div class="container-fluid mt-5">
		<div class="row">
			<div class="d-none d-sm-none d-md-block d-lg-block d-xl-block col-md-3 col-lg-3 col-xl-3">
				 <%@include file="menuIzq.jsp"%>
			</div>
			<div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9">
				<div id="mensajes"></div>
				<div class="card">
					<div class="card-body titulo">
						<%
							out.println("<h2 class='d-inline'>" + dtc.getNombre() + "</h2>");
							if (sesion.getAttribute("conectado") != null) {

								if (docente) {
						%>
						<button type="button" class="btn btn-success float-right d-inline"
							data-toggle="modal" data-target="#agregarEdicion">
							Agregar Edicion</button>

						<!-- MODAL AGREGAR CURSO -->
						<div class="modal fade" id="agregarEdicion" tabindex="-1"
							aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel">Agregar
											Edicion</h5>
										<button type="button" class="close" data-dismiss="modal"
											aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<form class="form" enctype="multipart/form-data" method="post">
											<div class="form-row">
												<div class="col">
													<div class="form-group">
														<label for="inputNombre">Nombre de Edicion</label> <input
															type="text" class="form-control" id="inputNombre"
															name="nombreEdicion" required>
													</div>
												</div>
											</div>

											<div class="form-row">
												<div class="col">
													<div class="form-group">
														<label for="inputFechaIni">Fecha de Inicio</label> <input
															type="date" class="form-control" id="inputFechaIni"
															name="FechaIni" value="" min="1900-01-01"
															max="2021-12-31" required>
													</div>
												</div>
												<div class="col">
													<div class="form-group">
														<label for="inputFechaFin">Fecha de Fin</label> <input
															type="date" class="form-control" id="inputFechaFin"
															name="FechaFin" value="" min="1900-01-01"
															max="2021-12-31" required>
													</div>
												</div>
											</div>
											<div class="form-row">
												<div class="col">
													<div class="form-group">
														<label for="inputFechaPub">Fecha de
															Publicaci&oacute;n</label> <input type="date"
															class="form-control" id="inputFechaPub" name="FechaPub"
															value="" min="1900-01-01" max="2021-12-31" required>
													</div>
												</div>
											</div>

											<div class="form-row">
												<div class="col">
													<div class="form-group">
														<label for=inputCupo>Cupo</label> <input type="text"
															class="form-control" id="inputCupo" name="cupo" required>
													</div>
												</div>
												<div class="col">
													<div class="form-group">
														<label for=inputImagenEdicion>Imagen: </label> <input
															type="file" name="inputImagenEdicion"
															id="inputImagenEdicion">
													</div>
												</div>
											</div>

											<div class="form-row">
												<div class="col">
													<div class="form-group">
														<label for="selectDocente">Docentes</label>
														<ul class="list-group">
															<%
																ArrayList<String> listDocente;
																		listDocente = (ArrayList<String>) session.getAttribute("listDocente");
																		System.out.println("Docente ---" + listDocente);
																		if (listDocente != null) {
																			System.out.println("Hay docentes");
																			for (int i = 0; i < listDocente.size(); i++) {

																				out.println("<li class='list-group-item'> " + listDocente.get(i)
																						+ "<input class='form-check-input float-right' type='checkbox' value="
																						+ listDocente.get(i) + " name='docente' id=" + listDocente.get(i) + "></li>");
																			}
																		} else {
																			out.println("<li class='list-group-item'> No hay datos cargados </li>");
																		}
															%>
														</ul>
													</div>
												</div>
											</div>

											<button type="button" class="btn btn-secondary"
												data-dismiss="modal">Cerrar</button>
											<button type="button" class="btn btn-primary"
												id="crearEdicion">Crear Edici&oacute;n</button>
										</form>
									</div>
								</div>
							</div>
						</div>
						<%
							}
							}
						%>
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="media">
							<div class="media-body">
								<div class="row">
									<div class="col-sm-4 text-center">
										<%
											if (dtc.getFoto() != null) {
												out.println("<img src='" + "imagenes/cursos/" + dtc.getFoto()
														+ "' class='img-curso img-fluid' alt=\"...\" >");
											} else {
												out.println(
														"<img src='imagenes/cursos/cursodefault.jpg' class='img-curso img-fluid' alt='Image not found' >");
											}
										%>
									</div>
									<div class="col-sm-8">
										<p>
											<span class="font-weight-bold">Duraci&oacute;n en
												Meses: </span>
											<%
												out.println(dtc.getDuracion());
											%>
										</p>
										<p>
											<span class="font-weight-bold">Duraci&oacute;n en
												Horas: </span>
											<%
												out.println(dtc.getCantidadHoras());
											%>
										</p>
										<p>
											<span class="font-weight-bold">URL: </span>
											<%
												out.println(dtc.getUrl());
											%>
										</p>
										<p>
											<span class="font-weight-bold">Cr&eacute;ditos: </span>
											<%
												out.println(dtc.getCantidadCreditos());
											%>
										</p>
										<%
											DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd"); //Quitar horas y segundos
											Date fechaA = dtc.getFechaAlta().getTime();
										%>
										<p>
											<span class="font-weight-bold">Fecha de Alta: </span>
											<%
												out.println(dateFormat.format(fechaA));
											%>
										</p>
									</div>
									<div class="col-sm-12">
										<hr>
										<%
											out.println("<p><b>Descripci&oacute;n: </b>" + dtc.getDescripcion() + "</p>");
										%>
									</div>
								</div>

							</div>
						</div>


						<div class="col-12">
							<!-- LISTA DE PREVIAS PARA ESTE CURSO -->
							<ul class="list-group">
								<li class="list-group-item subtitulo-card-previas">Previas</li>
								<%
									String[] dprevias = dtc.getDtprevias();
									boolean sinPrevias=false;
									if(dprevias==null){
										sinPrevias=true;
									}else{
										if (dprevias.length == 0)sinPrevias=true;
									}
									if (sinPrevias) {
										out.println("<li class='list-group-item'>");
										out.println("<p>No hay Previas para este Curso</p>");
										out.println("</li>");
									} else {
										for (String s : dprevias) {
											out.println("<li class='list-group-item'>" + s + "</li>");												
										}
									}
								%>

							</ul>


							<!-- LISTA DE EDICIONES PARA ESTE CURSO -->
							<ul class="list-group">
								<li class="list-group-item subtitulo-card-edicion">Ediciones</li>
								<%
									if (dtediciones.length == 0) {
										out.println("<li class='list-group-item'>");
										out.println("<p>No hay Ediciones cargadas</p>");
										out.println("</li>");
									} else {
										for (DtEdicion dte : dtediciones) {

											out.println("<li class='list-group-item'>");
											out.println("<a href='Edicion?i=" + nombreInstituto + "&amp;c=" + nombreCurso + "&amp;e="
													+ dte.getNombre() + "'>" + dte.getNombre() + "</a>");
											out.println("</li>");
										}
									}
								%>

							</ul>
						</div>
					</div>
				</div>

				<%
					DtEdicion dte = (DtEdicion) request.getAttribute("edi_vigente");
					DtInscripcionEdc[] inscripciones = (DtInscripcionEdc[]) request.getAttribute("inscripciones");
					DtInscripcionEdc[] inscripcionesp = (DtInscripcionEdc[]) request.getAttribute("inscripcionesp");
					Date ahora = new Date();
					boolean edicionValida = dte != null;
					boolean ultimaEdicionVencida;
					if (edicionValida) {
						ultimaEdicionVencida = ahora.compareTo(dte.getFechaF().getTime()) > 0; // si no se vencio da -1 si se vencio da +1
					} else {
						ultimaEdicionVencida = true;
					}
					if (edicionValida && docente) { // Aca es eldecido cuando ocultar las pesta�as de seleccionar estufiantes
				%>


				<div class="card">
					<div class="card-body">
						<div class="container">

							<br>
							<!-- Nav tabs -->
							<ul class="nav nav-tabs" role="tablist">
								<li class="nav-item">
									<%
										if (ultimaEdicionVencida) {
									%> <a class="nav-link active" data-toggle="tab" href="#home"><font
										color="red">Edicion Vencida</font></a>
								</li>
								<li class="nav-item"><a class="nav-link disabled"
									data-toggle="tab" href="#menu1">Seleccionar Estudiantes </a></li>
								<li class="nav-item"><a class="nav-link disabled"
									data-toggle="tab" href="#menu2">Por prioridad </a></li>
								<%
									} else {
								%>
								<a class="nav-link active" data-toggle="tab" href="#home">Edicion
									Activa</a>
								</li>
								<li class="nav-item"><a class="nav-link" data-toggle="tab"
									href="#menu1">Seleccionar Estudiantes </a></li>
								<li class="nav-item"><a class="nav-link" data-toggle="tab"
									href="#menu2">Por prioridad </a></li>
								<%
									}
								%>

							</ul>

							<!-- Tab panes -->
							<%
								out.println("  <h5></h5>");
							%>
							<div class="tab-content">
								<div id="home" class="container tab-pane active">
									<br>
									<!-- Aca va el contenido de Home -->
									<%
										out.println("<h6>Ultima Edicion vigente: " + dte.getNombre() + "</h6>");
									%>
									<%
										DateFormat dateFormat2 = new SimpleDateFormat("yyyy/MM/dd");
									%>
									<%
										out.println("<h6>Fecha de inicio: " + dateFormat2.format(dte.getFechaI().getTime()) + "</h6>");
									%>
									<%
										out.println("<h6>Fecha de finalizacion: " + dateFormat2.format(dte.getFechaF().getTime()) + "</h6>");
									%>
									<%
										out.println("<h6>Fecha de publicacion: " + dateFormat2.format(dte.getFechaPub().getTime()) + "</h6>");
									%>
									<%
										out.println("<h6>Cupo: " + dte.getCupo() + "</h6>");
									%>
								</div>
								<div id="menu1" class="container tab-pane fade">
									<br>
									<!--<form enctype="multipart/form-data"  onsubmit="submitForm(); return false;" id ="form">  -->

									<table style="width: 100%">
										<%
											int i = 1;
												//DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
												for (DtInscripcionEdc dti : inscripciones) {
													String is = Integer.toString(i);
													if (i == 1) {
														out.println("<tr>");
														out.println("  <th>Nombre</th>");
														out.println("  <th>Fecha Inscripcion</th>");
														out.println("  <th>Inscricion</th>");
														out.println("  <th>Estado</th>");
														out.println("</tr>");

													}
													publicadores.DtUsuario estudiante = dti.getEstudiante();
													out.println("<tr>");
													out.println("  <td>" + estudiante.getNombre() + " " + estudiante.getApellido()
															+ "</td>");

													out.println("  <td>" + dateFormat.format(dti.getFecha().getTime()) + "</td>");
													if (dti.getEstado() == EstadoInscripcionEdc.Inscripto) {
														out.println("  <td><input name='inscriptos' class='form-check-input' type='checkbox' value='"
																+ estudiante.getNickName() + "' id='defaultCheck" + is + "'></td>");
													} else {
														out.println("  <td><input class='form-check-input' type='checkbox' value='' id='defaultCheck"
																+ is + "' disabled></td>");
													}
													out.println("  <td>" + dti.getEstado().toString() + "</td>");
													out.println("</tr>");
													i++;

												}
										%>

									</table>
									<br>


									<button type="button" onclick="submitForm()"
										class="btn btn-success">Aceptar Estudiantes</button>


								</div>
								<div id="menu2" class="container tab-pane fade">
									<br>
									<table style="width: 100%">
										<%
											int j = 1;
												//DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
												for (DtInscripcionEdc dti : inscripcionesp) {
													String is = Integer.toString(j);
													if (j == 1) {
														out.println("<tr>");
														out.println("  <th>Nombre</th>");
														out.println("  <th>Fecha Inscripcion</th>");
														out.println("  <th>Inscricion</th>");
														out.println("  <th>Estado</th>");
														out.println("</tr>");

													}
													publicadores.DtUsuario estudiante = dti.getEstudiante();
													out.println("<tr>");
													out.println("  <td>" + estudiante.getNombre() + " " + estudiante.getApellido()
															+ "</td>");

													out.println("  <td>" + dateFormat.format(dti.getFecha().getTime()) + "</td>");
													if (dti.getEstado() == EstadoInscripcionEdc.Inscripto) {
														out.println("  <td><input name='inscriptos' class='form-check-input' type='checkbox' value='"
																+ estudiante.getNickName() + "' id='defaultCheck" + is + "'></td>");
													} else {
														out.println("  <td><input class='form-check-input' type='checkbox' value='' id='defaultCheck"
																+ is + "' disabled></td>");
													}
													out.println("  <td>" + dti.getEstado().toString() + "</td>");
													out.println("</tr>");
													j++;

												}
										%>

									</table>
									<br>


									<button type="button" onclick="submitForm()"
										class="btn btn-success">Aceptar Estudiantes</button>
								</div>

							</div>
						</div>
						<%
							}
						%>
					</div>
				</div>
			</div>

			<%@include file="footer.jsp"%>


			<script>
				function _(id) {
					return document.getElementById(id);
				}
				function submitForm() {
					//var inscriptos = document.forms[0];
					var checkboxes = document.getElementsByName('inscriptos');
					console.log(checkboxes[0].value);
					console.log(checkboxes[0].checked);
					var formData = new FormData();
					var ajax = new XMLHttpRequest();
					ajax.open("POST", "SleccionarEstudiantes", true);
					console.log("dos");
					ajax.onreadystatechange = function() {
						if (this.readyState == 4 && this.status == 200) {
							var r = this.responseText;

							document.getElementById("mensajes").innerHTML = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Mensaje: </strong> "
									+ r
									+ "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";

						}
						;
					}
					var selecciono = false;
					for (var i = 0; i < checkboxes.length; i++) {
						if (checkboxes[i].checked) {
							formData.append('aceptados', checkboxes[i].value);
							selecciono = true;
						}
					}
					if (selecciono) {
						ajax.send(formData);
					} else {
						document.getElementById("mensajes").innerHTML = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Mensaje: </strong> "
								+ "Debe seleccionar alguno"
								+ "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
						//alert("O selecciona alguno o no aprete el boton");
					}

				}
			</script>



			<script src="js/funciones_mostrar_curso.js"></script>
</body>
</html>