/**
 * Docente.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package publicadores;

public class Docente  extends publicadores.Usuario  implements java.io.Serializable {
    private java.lang.String nombreInsituto;

    private publicadores.Edicion[] edicionesInscripto;

    public Docente() {
    }

    public Docente(
           java.lang.String apellido,
           java.lang.String correo,
           java.lang.Boolean esDocente,
           java.util.Calendar fechaNacimiento,
           java.lang.String imagen,
           byte[] img,
           publicadores.InscripcionEdc[] inscripciones,
           java.lang.String nickName,
           java.lang.String nombre,
           java.lang.String password,
           publicadores.Usuario[] usuariosSeguidos,
           java.lang.String nombreInsituto,
           publicadores.Edicion[] edicionesInscripto) {
        super(
            apellido,
            correo,
            esDocente,
            fechaNacimiento,
            imagen,
            img,
            inscripciones,
            nickName,
            nombre,
            password,
            usuariosSeguidos);
        this.nombreInsituto = nombreInsituto;
        this.edicionesInscripto = edicionesInscripto;
    }


    /**
     * Gets the nombreInsituto value for this Docente.
     * 
     * @return nombreInsituto
     */
    public java.lang.String getNombreInsituto() {
        return nombreInsituto;
    }


    /**
     * Sets the nombreInsituto value for this Docente.
     * 
     * @param nombreInsituto
     */
    public void setNombreInsituto(java.lang.String nombreInsituto) {
        this.nombreInsituto = nombreInsituto;
    }


    /**
     * Gets the edicionesInscripto value for this Docente.
     * 
     * @return edicionesInscripto
     */
    public publicadores.Edicion[] getEdicionesInscripto() {
        return edicionesInscripto;
    }


    /**
     * Sets the edicionesInscripto value for this Docente.
     * 
     * @param edicionesInscripto
     */
    public void setEdicionesInscripto(publicadores.Edicion[] edicionesInscripto) {
        this.edicionesInscripto = edicionesInscripto;
    }

    public publicadores.Edicion getEdicionesInscripto(int i) {
        return this.edicionesInscripto[i];
    }

    public void setEdicionesInscripto(int i, publicadores.Edicion _value) {
        this.edicionesInscripto[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Docente)) return false;
        Docente other = (Docente) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.nombreInsituto==null && other.getNombreInsituto()==null) || 
             (this.nombreInsituto!=null &&
              this.nombreInsituto.equals(other.getNombreInsituto()))) &&
            ((this.edicionesInscripto==null && other.getEdicionesInscripto()==null) || 
             (this.edicionesInscripto!=null &&
              java.util.Arrays.equals(this.edicionesInscripto, other.getEdicionesInscripto())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getNombreInsituto() != null) {
            _hashCode += getNombreInsituto().hashCode();
        }
        if (getEdicionesInscripto() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getEdicionesInscripto());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getEdicionesInscripto(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Docente.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://publicadores/", "docente"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nombreInsituto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nombreInsituto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("edicionesInscripto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "edicionesInscripto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://publicadores/", "edicion"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
