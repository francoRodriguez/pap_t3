/**
 * ControladorPublish.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package publicadores;

public interface ControladorPublish extends java.rmi.Remote {
    public void altaUsuarioDocente(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.util.Calendar arg4, boolean arg5, java.lang.String arg6, java.lang.String arg7, java.lang.String arg8) throws java.rmi.RemoteException, publicadores.EmailExistenteException, publicadores.NickNameExistenteException;
    public void altaUsuarioEstudiante(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.util.Calendar arg4, boolean arg5, java.lang.String arg6, java.lang.String arg7) throws java.rmi.RemoteException, publicadores.EmailExistenteException, publicadores.NickNameExistenteException;
    public java.lang.String rutaAnombreDeImagen(java.lang.String arg0) throws java.rmi.RemoteException;
    public java.lang.String[] listarInstitutosArray() throws java.rmi.RemoteException;
    public boolean nuevoCurso(java.lang.String arg0) throws java.rmi.RemoteException;
    public void setearCurso(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, int arg3, int arg4, java.lang.String arg5, java.util.Calendar arg6, java.lang.String arg7) throws java.rmi.RemoteException;
    public java.lang.String[] listarNombreCursos(java.lang.String arg0) throws java.rmi.RemoteException;
    public publicadores.DtUsuario seleccionarDtUsuario(java.lang.String arg0) throws java.rmi.RemoteException;
    public java.lang.String[] listarEdicionesdeCurso(java.lang.String arg0) throws java.rmi.RemoteException;
    public publicadores.Usuario seleccionarUsuario(java.lang.String arg0) throws java.rmi.RemoteException;
    public java.lang.String[] seleccionarInscripcionED(java.lang.String arg0) throws java.rmi.RemoteException;
    public java.lang.String[] listarProgramas() throws java.rmi.RemoteException;
    public java.lang.String[] listarUsuarios() throws java.rmi.RemoteException;
    public publicadores.DtCurso seleccionarCurso(java.lang.String arg0) throws java.rmi.RemoteException;
    public boolean modificarDatos(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.util.Calendar arg4) throws java.rmi.RemoteException;
    public java.lang.String[] cursosDePrograma() throws java.rmi.RemoteException;
    public boolean loginEmail(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public boolean loginNickname(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException, publicadores.UsuarioNoExisteException;
    public void setearCategorias(java.lang.String[] arg0) throws java.rmi.RemoteException;
    public publicadores.DtInscripcionEdc[] inscrpcionesAceptadas(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2) throws java.rmi.RemoteException;
    public void setearInscripciones(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, publicadores.DtInscripcionEdc[] arg3) throws java.rmi.RemoteException;
    public publicadores.DtEdicion traerUltimaEdicion(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException, publicadores.SinEdicionException;
    public publicadores.DtInscripcionEdc[] obtenerInscripciones(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, boolean arg3) throws java.rmi.RemoteException;
    public void confirmarAltaCurso() throws java.rmi.RemoteException;
    public publicadores.DtCurso mostrarDatosdeCurso(java.lang.String arg0) throws java.rmi.RemoteException;
    public publicadores.DtEdicion[] mostrarDtEdicionesArray(java.lang.String arg0) throws java.rmi.RemoteException;
    public void seleccionarPrevias(java.lang.String[] arg0) throws java.rmi.RemoteException;
    public void altaInscripcionEdc(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException, publicadores.InscripcionEdicionRepetida;
    public void seleccionarCursoInstancia(java.lang.String arg0) throws java.rmi.RemoteException;
    public void seleccionarInstituto(java.lang.String arg0) throws java.rmi.RemoteException;
    public publicadores.ArrayList listarCategoriasList() throws java.rmi.RemoteException;
    public java.lang.String[] listarCategoriasPF(java.lang.String arg0) throws java.rmi.RemoteException;
    public publicadores.DtDocente seleccionarDtDocente(java.lang.String arg0) throws java.rmi.RemoteException;
    public publicadores.DtCurso[] cursosdeCategoria(java.lang.String arg0) throws java.rmi.RemoteException;
    public void dejarSeguirUsuario(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException, publicadores.Exception;
    public void altaPrograma(java.lang.String arg0, java.lang.String arg1, java.util.Calendar arg2, java.util.Calendar arg3, java.lang.String arg4) throws java.rmi.RemoteException, publicadores.IOException;
    public boolean checkSeguidor(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public java.lang.String[] listarCategorias() throws java.rmi.RemoteException;
    public java.lang.String[] usuariosSeguidos(java.lang.String arg0) throws java.rmi.RemoteException, publicadores.IOException;
    public publicadores.DtPrograma verPrograma(java.lang.String arg0) throws java.rmi.RemoteException;
    public void pruebaDate(java.util.Calendar arg0) throws java.rmi.RemoteException;
    public java.lang.String[] listarDocentes() throws java.rmi.RemoteException;
    public void seguirUsuario(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException, publicadores.Exception;
    public java.lang.String institutoDeCurso(java.lang.String arg0) throws java.rmi.RemoteException;
    public boolean altaEdicionCurso(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.util.Calendar arg3, java.util.Calendar arg4, int arg5, java.util.Calendar arg6, publicadores.Docente[] arg7, java.lang.String arg8) throws java.rmi.RemoteException;
    public publicadores.DtEdicion mostrarEdicion(java.lang.String arg0) throws java.rmi.RemoteException;
    public publicadores.DtPrograma[] programasdeCurso(java.lang.String arg0) throws java.rmi.RemoteException;
    public java.lang.String emailToNick(java.lang.String arg0) throws java.rmi.RemoteException;
}
