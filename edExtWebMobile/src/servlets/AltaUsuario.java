package servlets;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import javax.xml.rpc.ServiceException;

import publicadores.EmailExistenteException;
import publicadores.NickNameExistenteException;

import publicadores.ControladorPublish;
import publicadores.ControladorPublishService;
import publicadores.ControladorPublishServiceLocator;

/**
 * Servlet implementation class AltaUsuario
 */
@MultipartConfig(fileSizeThreshold = 1024 * 1024, maxFileSize = 1024 * 1024 * 5, maxRequestSize = 1024 * 1024 * 5 * 5)


@WebServlet("/AltaUsuario")
public class AltaUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public AltaUsuario() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		RequestDispatcher rd;
		System.out.println("estoy en dopost arriba");
		HttpSession sesion = request.getSession(false);
		
		String nick = request.getParameter("nickName") ;
		String email= request.getParameter("email");
		String nombre= request.getParameter("nombre");
		String apellido= request.getParameter("apellido");
		String imagen;
		String checkbok= request.getParameter("esDocente");
		Boolean esDocente;
		String password= request.getParameter("password");
		if(checkbok==null) {
			esDocente=false;
		}else {
			esDocente=true;
		}
		String nombreInstituto= request.getParameter("instituto");
		
		//fecha 
		String inputDate = request.getParameter("inputFechaNac");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar fechaN= sdf.getCalendar();
		
			/*try {
				fechaN= sdf.parse(inputDate);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/		   
		
		//carga de imagen
			String foto;
			Part archivo = request.getPart("imagenPerfil"); //llamada al parámetro foto de mi formulario.
			if(archivo.getSize()>0) {
				String fotoName =  archivo.getSubmittedFileName();
				String fotoRuta = System.getProperty("user.home") + File.separator + "edExt"+ File.separator +"images"  + File.separator + fotoName;
				archivo.write(fotoRuta); // Escribimos el archivo al disco duro del servidor.
				foto= fotoRuta;
			}else {
				String fotoRuta = System.getProperty("user.home") + File.separator + "edExt"+ File.separator +"images"  + File.separator + "desconocido.jpg";
				foto=fotoRuta;
			}
			
			System.out.println("es Docnete? " + esDocente);
			//	llamo a mi interfaz para dar de alta el usuario
			if(esDocente == false) {
				try {
					System.out.println("entra");
					altaUsuarioEstudiante(nick, nombre, apellido, email, fechaN, esDocente, password, foto);
					String mensaje = "Bienvenido a EdExt " + nombre + " " + apellido +" por favor inicia sesion.";
					request.setAttribute("mensajeOK", mensaje);
					rd = request.getRequestDispatcher("index.jsp");
					rd.forward(request, response);
				} catch (NickNameExistenteException | EmailExistenteException | ServiceException e) {
					System.out.println("entra 2");
					// TODO Auto-generated catch block
					e.printStackTrace();
					String mensaje = e.getMessage();
					request.setAttribute("mensajeError", mensaje);
					rd = request.getRequestDispatcher("index.jsp");
					rd.forward(request, response);
				}
			}
			else {
				try {
					try {
						altaUsuarioDocente(nick, nombre, apellido, email, fechaN, esDocente, password, foto, nombreInstituto);
					} catch (ServiceException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					String mensaje = "Bienvenido a EdExt " + nombre + " " + apellido +" por favor inicia sesion."; 					
					request.setAttribute("mensajeOK", mensaje);
					rd = request.getRequestDispatcher("index.jsp");
					rd.forward(request, response);
				} catch (NickNameExistenteException | EmailExistenteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					String mensaje = e.getMessage();
					request.setAttribute("mensajeError", mensaje);
					rd = request.getRequestDispatcher("index.jsp");
					rd.forward(request, response);
				}
				
			}
			
	}
	
	//OPERACIONES CONSUMIDAS	
	public void altaUsuarioEstudiante(String nickName, String nombre, String apellido, String correo, Calendar fechaNacimiento, Boolean esDocente, String password, String imagen) throws NickNameExistenteException, EmailExistenteException, ServiceException{
			ControladorPublishService cps = new ControladorPublishServiceLocator();
			ControladorPublish port = cps.getControladorPublishPort();
			try {
				port.altaUsuarioEstudiante(nickName, nombre, apellido, correo, fechaNacimiento, esDocente, password, imagen);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
	public void altaUsuarioDocente(String nickName, String nombre, String apellido, String correo, Calendar fechaNacimiento,	Boolean esDocente,  String password, String imagen, String nombreInstituto) throws NickNameExistenteException, EmailExistenteException, ServiceException {
		ControladorPublishService cps = new ControladorPublishServiceLocator();
		ControladorPublish port = cps.getControladorPublishPort();
		try {
			port.altaUsuarioDocente(nickName, nombre, apellido, correo, fechaNacimiento, esDocente, password, imagen, nombreInstituto);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
