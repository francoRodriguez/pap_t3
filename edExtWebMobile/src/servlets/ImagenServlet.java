package servlets;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import publicadores.ControladorPublish;
import publicadores.ControladorPublishService;
import publicadores.ControladorPublishServiceLocator;

/**
 * Servlet implementation class Imagen
 */
@WebServlet(name = "ImageServlet", value = "/ImageServlet/*")

public class ImagenServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ImagenServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		
		response.getWriter().append("Served at: ").append(request.getContextPath());		

		ControladorPublishService cps = new ControladorPublishServiceLocator();
		try {
			ControladorPublish port 	= cps.getControladorPublishPort();
			publicadores.DtUsuario u = port.seleccionarDtUsuario(request.getParameter("nick"));
			String fotoRuta = u.getImagen();
			
			response.setContentType("image/jpeg");  
			File file = new File( fotoRuta);
			BufferedImage binarioImagen = ImageIO.read(file);
			OutputStream respuesta = response.getOutputStream();
			ImageIO.write(binarioImagen, "jpg", respuesta);
			respuesta.close();
			
			doGet(request, response);
		}catch(ServiceException e) {
			String text = "Error en  getControladorPublishPort AltaProgramaDeFormacion.java";
			response.setContentType("text/plain");
			response.getWriter().write(text);
		}
		
		
		

		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

}
