package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import publicadores.ControladorPublish;
import publicadores.ControladorPublishService;
import publicadores.ControladorPublishServiceLocator;




/**
 * Servlet implementation class PruebAjax
 */
@MultipartConfig
@WebServlet("/PruebAjax")
public class PruebAjax extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PruebAjax() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 //TODO Auto-generated method stub
		String text = "Texto del servlet";
		response.setContentType("text/plain");
		response.getWriter().write(text);
		ControladorPublishService cps = new ControladorPublishServiceLocator();
		try {
			ControladorPublish port = cps.getControladorPublishPort();
			String[] categorias = port.listarCategorias();
			for(String s:categorias) {
				System.out.println(s);
			}
		}catch(ServiceException e) {
			System.out.println("Service Exception");
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		/*String nombre = (String)request.getParameter("nombre") ;
		String descrip = (String)request.getParameter("descrip") ;
		System.out.println("Servlet: " + nombre + " "+ descrip );
		String text = "Exito!";
		response.setContentType("text/plain");
		response.getWriter().write(text);
		Enumeration paramNames = request.getParameterNames();
		while(paramNames.hasMoreElements()) {
		      String paramName = (String)paramNames.nextElement();
		      String[] paramValues = request.getParameterValues(paramName);
		        //for(int i=0; i<paramValues.length; i++) {
		            System.out.println("servlet: " + (String)request.getParameter(paramName));
		          //}
		}*/

		
		//ArrayList<String> categorias= (ArrayList<String>)port.listarCategorias();
		
	}

}
