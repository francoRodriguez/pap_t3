package servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import publicadores.ControladorPublish;
import publicadores.ControladorPublishService;
import publicadores.ControladorPublishServiceLocator;

/**
 * Servlet implementation class ModificarUsuario
 */
@WebServlet("/ModificarUsuario")
public class ModificarUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModificarUsuario() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		String nickName 	= request.getParameter("nickName");
		String nombre		= request.getParameter("nombre");
		String apellido		= request.getParameter("apellido");
		String email		= request.getParameter("email");
		String fechaNac		= request.getParameter("fechaNac");
		
		
		System.out.println("nickName: " + nickName);
		System.out.println("nombre: " + nombre);
		System.out.println("apellido: " + apellido);
		System.out.println("email: " + email);
		System.out.println("fechaNac: " + fechaNac);
		
		Calendar fecha = Calendar.getInstance();
		Date d = new Date();
		try {
			fecha.setTime(d);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			fecha.setTime(sdf.parse(fechaNac));

			System.out.println(fecha.getTime().toString());
			
			
			
			//fecha = new SimpleDateFormat("yyyy-MM-dd").parse(fechaNac);
			//System.out.println(fecha);
			//fechaConv.set(fecha.getYear(), fecha.getMonth(), fecha.getDate());
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		RequestDispatcher rd;
		if(nombre.isEmpty() || apellido.isEmpty() || fechaNac.isEmpty()) {
			System.out.println("Campos vacios");
			String mensaje = "No puedes tener campos vac&iacute;os.";
			request.setAttribute("mensajeError", mensaje);
			request.setAttribute("hayError", true);
			
			rd = request.getRequestDispatcher("consultaUsuario.jsp");
            rd.forward(request,response);
		}
		else {
			//boolean res = iconu.modificarDatos(nickName, nombre, apellido, email, fechaConv);
			//System.out.println("Resultado: " + res);
			try {
				modificarDatos(nickName, nombre, apellido, email, fecha);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        HttpSession miSesion = request.getSession(true);
	        miSesion.setAttribute("nombre", nombre);
	        miSesion.setAttribute("apellido", apellido);
	        miSesion.setAttribute("fechaNacimiento", fecha);
	        
	        rd = request.getRequestDispatcher("consultaUsuario.jsp");
	        rd.forward(request,response);
		}
		
	}
	
	public void modificarDatos(String nickName, String nombre, String apellido, String email, Calendar fecha) throws Exception{
		ControladorPublishService cps = new ControladorPublishServiceLocator();
		ControladorPublish port = cps.getControladorPublishPort();
		port.modificarDatos(nickName, nombre, apellido, email, fecha);
	}

}
