package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import publicadores.ControladorPublish;
import publicadores.ControladorPublishService;
import publicadores.ControladorPublishServiceLocator;
import publicadores.DtEdicion;
import publicadores.DtInscripcionEdc;
import publicadores.SinEdicionException;

/**
 * Servlet implementation class ContinuarCurso
 */


@WebServlet("/ContinuarCurso")
public class ContinuarCurso extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static String instituto;
	private static String curso;
	private static DtEdicion ultimaedicion;
	private static DtInscripcionEdc[] inscripcioness;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ContinuarCurso() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		ControladorPublishService cps = new ControladorPublishServiceLocator();
		try {
				ControladorPublish port = cps.getControladorPublishPort();
			String insSel= (String) request.getAttribute("insSel");
			instituto=insSel;   //static
			String curSel= (String) request.getParameter("curselecc");
			curso=curSel;
			System.out.println("ContinuarCurso insel " + insSel);
			
			try {
				DtEdicion dte = port.traerUltimaEdicion(curSel, insSel);
				ultimaedicion=dte;
				
				DtInscripcionEdc[] inscripciones = port.obtenerInscripciones(curSel, insSel, dte.getNombre(),false); // DtInscripcionEdc[] 
				DtInscripcionEdc[] inscripcionesp = port.obtenerInscripciones(curSel, insSel, dte.getNombre(),true);
				System.out.println("Nun inscripcione "+inscripciones.length+ " "+dte.getNombre());
				inscripcioness=inscripciones;   //static
				if(inscripciones.length >= 0) {
					request.setAttribute("edi_vigente", dte);
					request.setAttribute("inscripciones",inscripciones);
					request.setAttribute("inscripcionesp",inscripcionesp);
					for(DtInscripcionEdc dti:inscripciones ) {
						System.out.println("ContinuarCurso " + dti.getEdicion().getNombre()+ " "+ dti.getEstudiante().getNickName());
					}
				}else {
	
				}
				
			}catch(SinEdicionException e1){
				System.out.println("Sin ediciones ContCurso");
				request.setAttribute("excep1","No hay ediciones");
			}
			///
			RequestDispatcher rd;
			rd = request.getRequestDispatcher("mostrarCurso.jsp");
			rd.forward(request, response);
			//
		}catch(ServiceException e) {
			System.out.println("Service Exception");
		}			
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		
	}

	public static String getInstituto() {
		return instituto;
	}

	public static String getCurso() {
		return curso;
	}

	public static DtEdicion getUltimaedicion() {
		return ultimaedicion;
	}

	public static DtInscripcionEdc[] getInscripcioness() {
		return inscripcioness;
	}
	
}
