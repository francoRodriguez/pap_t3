package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.rpc.ServiceException;

import publicadores.DtCurso;
import publicadores.DtEdicion;
import publicadores.ControladorPublish;
import publicadores.ControladorPublishService;
import publicadores.ControladorPublishServiceLocator;

/**
 * Servlet implementation class Cursos
 */
@WebServlet("/Cursos")
public class Cursos extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Cursos() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub		
		ControladorPublishService cps = new ControladorPublishServiceLocator();
		try {
			ControladorPublish port = cps.getControladorPublishPort();
			String insSel= (String) request.getParameter("inselecc");
			String curSel= (String) request.getParameter("curselecc");
			String catSel= (String) request.getParameter("catselecc");
			
			HttpSession sesion;         
	        sesion = request.getSession(false);
	        sesion.setAttribute("status", "none");
			
			//response.getWriter().append("Served at: ").append(request.getContextPath());
			//response.getWriter().append("Served at: "+insSel+ " " + curSel+"\n");
			if(!insSel.equals("null")) {
				String[] listaCursos = port.listarNombreCursos(insSel);
				DtCurso dtc = port.mostrarDatosdeCurso(curSel);
				System.out.println(dtc.getNombre());
				System.out.println(dtc.getFoto());
				System.out.println(dtc.getDescripcion());
				DtEdicion[] dtediciones=port.mostrarDtEdicionesArray(dtc.getNombre());
				
				request.setAttribute("dtediciones", dtediciones);
				request.setAttribute("dtCurso", dtc);
				
				request.setAttribute("insSel", insSel);
				request.setAttribute("curSel", curSel);
				request.setAttribute("catSel", catSel);
				
				RequestDispatcher rd;
				rd = request.getRequestDispatcher("ContinuarCurso");//mostrarCurso.jsp
				rd.forward(request, response);
			}else {
				String instDelCurso= port.institutoDeCurso(curSel);
				if(instDelCurso!=null) {
					String[] listaCursos = port.listarNombreCursos(instDelCurso);
					DtCurso dtc = port.mostrarDatosdeCurso(curSel);
					System.out.println("En Cursos Java " + instDelCurso);
					//System.out.println(dtc.getFoto());
					//System.out.println(dtc.getDescripcion());
					DtEdicion[] dtediciones=port.mostrarDtEdicionesArray(dtc.getNombre());
					
					request.setAttribute("dtediciones", dtediciones);
					request.setAttribute("dtCurso", dtc);				
					request.setAttribute("insSel", instDelCurso);
					request.setAttribute("curSel", curSel);
					request.setAttribute("sininstituto", "si");
					RequestDispatcher rd;
					rd = request.getRequestDispatcher("ContinuarCurso");//mostrarCurso.jsp
					rd.forward(request, response);
				}else {
					response.getWriter().append("Lo siento, el caso de uso requiere que seleccione un Instituto");
				}
				
			}
		}catch(ServiceException e) {
			System.out.println("Service Exception");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
