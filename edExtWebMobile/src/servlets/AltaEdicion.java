package servlets;

import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import javax.xml.rpc.ServiceException;


import publicadores.ControladorPublish;
import publicadores.ControladorPublishService;
import publicadores.ControladorPublishServiceLocator;

/**
 * Servlet implementation class AltaEdicion
 */
@MultipartConfig(fileSizeThreshold = 1024 * 1024, maxFileSize = 1024 * 1024 * 5, maxRequestSize = 1024 * 1024 * 5 * 5)

@WebServlet("/AltaEdicion")
public class AltaEdicion extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AltaEdicion() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		
		
		Part archivo = request.getPart("img");
		String foto =  archivo.getSubmittedFileName();
		String fotoRuta = System.getProperty("user.home") + File.separator + "edExt"+ File.separator +"images"  + File.separator + "cursos"+ File.separator + foto;
		archivo.write(fotoRuta);
		String edicion	= (String)request.getParameter("nombreEdicion"); 
		//String img= (String)request.getParameter("img");
		//System.out.println("Edicion: " + edicion);
		String arregloDocentes	= (String)request.getParameter("arregloDocentes");
		//System.out.println("doc: " + arregloDocentes);
		String fechaInicio	= (String)request.getParameter("fechaInicio");
		//System.out.println("fechaInicio: " + fechaInicio);
		String fechaFin	= (String)request.getParameter("fechaFin");
		//System.out.println("fechaFin: " + fechaFin);
		String	 cupoS	= request.getParameter("cupo");
		int cupo = Integer.parseInt(cupoS);
		//System.out.println("cupo: " + cupo);
		
		String idCurso= (String)request.getParameter("idCurso");
		//System.out.println("idCurso: " + idCurso);
		String idInstituto= (String)request.getParameter("idInstituto");
		//System.out.println("idInstituto: " + idInstituto);
		String fechaPub= (String)request.getParameter("fechaPub");
		//System.out.println("fechaPub: " + fechaPub);
		
		List<String> listaDocentes = Arrays.asList(arregloDocentes.split("\\s*,\\s*"));
		
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date fechaInicioFormat= null;		
		try {
			fechaInicioFormat= sdf.parse(fechaInicio);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Date fechaFinFormat= null;		
		try {
			fechaFinFormat= sdf.parse(fechaFin);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}	
		Date fechaPubFormat= null;		
		try {
			fechaPubFormat= sdf.parse(fechaPub);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}	
		
		Calendar fechaInicioG= dateToCalendar(fechaInicioFormat);
		
		Calendar fechaFinG= dateToCalendar(fechaFinFormat);
		
		Calendar fechaPubG= dateToCalendar(fechaPubFormat);
			
		
		 publicadores.Docente docentes[] = new publicadores.Docente[listaDocentes.size()];
		
		for(int i=0; i< listaDocentes.size(); i++) {
			String nombre=listaDocentes.get(i);
			publicadores.DtDocente doc = null;
			try {
				doc = seleccionarDtDocente(nombre);
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			publicadores.Docente d=  new publicadores.Docente();

			d.setNickName(doc.getNickName());
			d.setNombre(doc.getNombre());
			d.setApellido(doc.getApellido());
			d.setCorreo(doc.getCorreo());
			d.setFechaNacimiento(doc.getFechaNacimiento());
			d.setEsDocente(doc.isEsDocente());
			d.setPassword(null);
			d.setImagen(doc.getImagen());
			d.setImg(null);
			d.setUsuariosSeguidos(null);
			d.setInscripciones(null);
			d.setNombreInsituto(doc.getNombreInsituto());
			
			docentes[i]=d;

			
		}
		

		
		
		Boolean res = false;
		if (docentes.length > 0) {
			
			try {
				res = altaEdicionCurso(idInstituto, idCurso, edicion, fechaInicioG, fechaFinG, cupo, fechaPubG, docentes, foto);
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else {
			res=false;
		}
		
		
		
		if(res) {
			String text = "ok?Se dio de alta la edicion "+ edicion;
			response.setContentType("text/plain");
			response.getWriter().write(text);
		} else {
			String text = "error?Ya existe una edicion con el nombre "+ edicion;
			response.setContentType("text/plain");
			response.getWriter().write(text);
		}
		
		
		
	}
	
	//METODOS QUE CONSUMO
	
	public boolean altaEdicionCurso(String nombreInstituto, String nombreCurso, String nombreEdicion, Calendar fechaI, Calendar fechaF,	int cupo, Calendar fechaPub, publicadores.Docente[] docentes, String imagen) throws ServiceException, RemoteException {
		
		ControladorPublishService cps = new ControladorPublishServiceLocator();
		ControladorPublish port = cps.getControladorPublishPort();
		return port.altaEdicionCurso( nombreInstituto, nombreCurso, nombreEdicion, fechaI, fechaF, cupo,  fechaPub, docentes, imagen);
		
	}
	
	public publicadores.DtDocente seleccionarDtDocente(String docente77) throws ServiceException, RemoteException {
		ControladorPublishService cps = new ControladorPublishServiceLocator();
		ControladorPublish port = cps.getControladorPublishPort();
		return  port.seleccionarDtDocente(docente77);
	}
	
	private Calendar dateToCalendar(Date date) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;

    }
	

}
