package servlets;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.xml.rpc.ServiceException;

import publicadores.ControladorPublish;
import publicadores.ControladorPublishService;
import publicadores.ControladorPublishServiceLocator;

/**
 * Servlet implementation class AltaCurso
 */
@MultipartConfig(fileSizeThreshold = 1024 * 1024, maxFileSize = 1024 * 1024 * 5, maxRequestSize = 1024 * 1024 * 5 * 5)

@WebServlet("/AltaCurso")
public class AltaCurso extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AltaCurso() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		ControladorPublishService cps = new ControladorPublishServiceLocator();
		try {
			ControladorPublish port = cps.getControladorPublishPort();
			String nombre = (String)request.getParameter("nomCurso") ; //System.out.println(nombre);
			String desc = (String)request.getParameter("desc") ;
			String duracion = (String)request.getParameter("dur") ;
			String horas = (String)request.getParameter("horas") ;
			String creditos = (String)request.getParameter("creditos") ;
			int canthoras = Integer.parseInt(horas);
			int creditosint= Integer.parseInt(creditos);
			String url = (String)request.getParameter("url") ;
			String[] previas= (String[])request.getParameterValues("previas");
			String[] categorias= (String[])request.getParameterValues("programas");//esta mal llamado programas
			//for(String s:previas)System.out.println(s);
			String instituto = (String)request.getParameter("instituto"); System.out.println(instituto);
			port.seleccionarInstituto(instituto);
			System.out.println("Salio SeleccInsti ");
			boolean ok = port.nuevoCurso(nombre);
			System.out.println("Salio nuevoCurso ");
			
			if(!ok) {
				//response.getWriter().append("Error ya existe el curso: ").append(request.getContextPath());
				String text = "Error :( ";
				response.setContentType("text/plain");
				response.getWriter().write(text);
			}else {
				Part archivo = request.getPart("imagenCurso");
				String foto;
				if(archivo.getSize()>0) {
					 foto =  archivo.getSubmittedFileName();				
					String fotoRuta = System.getProperty("user.home") + File.separator + "edExt"+ File.separator +"images"  + File.separator + "cursos"+ File.separator + foto;
					foto= archivo.getSubmittedFileName();//fotoRuta; //preguntar porque estaba eso, a mi no me anda asi
					archivo.write(fotoRuta);
				}else {
					String fotoRuta = System.getProperty("user.home") + File.separator + "edExt"+ File.separator +"images"  + File.separator + "cursos"+ File.separator + "cursodefault.jpg";
					foto="cursodefault.jpg";
				}
				Date d = new Date();
				Calendar c=Calendar.getInstance() ;
				c.setTime(d);
				port.setearCurso(nombre, desc, duracion, canthoras, creditosint, url, c , foto);
				if(previas!=null&&previas.length>0) {
					//ArrayList<String> listprevias= new ArrayList<>();
					//for(String p: previas) listprevias.add(p);
					port.seleccionarPrevias(previas);	//iba listprevias

				}
				if(categorias!=null&&categorias.length>0) {
					ArrayList<String> categoriasdelcurso = new ArrayList<>();//no se usa
					for(String cat: categorias) categoriasdelcurso.add(cat);//legacy entrega2   
					port.setearCategorias(categorias);
				}
				System.out.println("Termino AltaCurso");
				port.confirmarAltaCurso();
				//response.getWriter().append("Exito: ").append(request.getContextPath());
				String text = "Exito!";
				response.setContentType("text/plain");
				response.getWriter().write(text);
				
				
			}
		}catch(ServiceException e) {
			System.out.println("Service Exception");
		}		
		
	}
	

}
